<html>
    <head>
        @if(Session::has('access_token'))
        <script type="text/javascript">
            localStorage.setItem("access_token", "{{ Session::get('access_token') }}" )
        </script>
        @endif
        <script type="text/javascript">
            window.location.href = "{{ url('/') }}";
        </script>
    </head>
</html>
