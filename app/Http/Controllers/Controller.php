<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Queue\Failed\NullFailedJobProvider;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){

        $data = DB::table($this->table)->get();

        return $this->toArray($data);
    }

    public function show($id){

        $data = DB::table($this->table)->where('id',$id)->get();

        return $this->toArray($data);
    }

    public function destroy($id){

        $data = DB::table($this->table)->where('id',$id)->delete();

        $this->toArray($data, 204);
    }

    public function checkData($table, $key, $val){

        $data = DB::table($table)->where($key, $val)->get()->count();

        if ($data < 1) {
            return false;
        }

        return true;
    }

    public function toArray($data){

        try {
            $response = ['data' => $data];
            $status = 200;

            if (count($data) < 1) {
                $response = ['message' => 'Data Not Found'];
                $status = 404;
            }
    
        }
        catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status   = $exception->getStatusCode();
        }
    
        return response()->json($response, $status);
    }

    public function httpResponse($r=NULL,$s=NULL,$d=NULL){
        if($r === 'exist' && $s === true)
        {
            return response()->json(['message'=>'Data already exist'], 400);
        }
        else if($r === 'exist' && $s === false)
        {
            return response()->json(['message'=>'Data not found'], 404);
        }
        else if($r === 'insert' && $s === true)
        {
            return response()->json(['data' => $d, 'message' => 'Data has been created' ], 201);
        }
        else if($r === 'insert' && $s === false)
        {
            return response()->json(['message' => 'Data not been created'], 400);
        }
        else if($r === 'update' && $s === true)
        {
            return response()->json(['data' => $data, 'message' => 'Data has been changed'], 200);
        }
        else if($r === 'update' && $s === false)
        {
            return response()->json(['message' => 'Data not been created'], 200);
        }
    }

}
