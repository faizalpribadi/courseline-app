<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Register | Courseline - Belajar menjadi lebih mudah</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/extra-style.css') }}" >
</head>
<body>
<div class="wrapper" style="padding:50px 0;">

    <a href="{{ asset('/') }}" class="btn btn-back"> <i class="fa fa-arrow-left"></i><span>Back to Courseline</span></a>

    <div class="box-wrapper col-sm-6 col-xs-8 col-centered">
        <div class="col-sm-12 col-md-7 col-lg-7 vcenter">
            <div class="row">
                <div class="col-sm-12" style="margin-bottom: 40px;">
                    <img src="{{ asset('img/extra/logo.png') }}" width="40%"/>
                    <h3>Create your Courseline Account</h3>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-simple">
                        <label>First Name</label>
                        <input type="text" class="form-control" id="firstName"/>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-simple">
                        <label>Last Name</label>
                        <input type="text" class="form-control" id="lastName"/>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-simple">
                        <label>Email</label>
                        <input type="text" class="form-control" id="email"/>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group form-simple">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" />
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group form-simple">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" id="confirmPassword"/>
                    </div>
                </div>

                <div class="col-sm-1">
                    <i class="fa fa-eye-slash" id="showPassword"></i>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-simple">
                        <label>Course Category</label>
                        <select class="form-control">
                            <option>Vocaline</option>
                            <option>Businessline</option>
                            <option>Languageline</option>
                            <option>Schooline</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-simple">
                        <label>Course Level</label>
                        <select class="form-control">
                            <option>Beginner</option>
                            <option>Intermediate</option>
                            <option>Advance</option>
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="row" style="margin-top: 40px;">
                <div class="disp-tbl">
                    <div class="col-sm-8 col-lg-8 col-md-8 vcenter">
                        <a href="{{ asset('login') }}">Sign in instead</a>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4 vcenter">
                        <button class="btn btn-primary pull-right" id="regBtn">Register</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-5 hidden-xs hidden-sm text-center vcenter">
            <img src="{{ asset('img/graduation.jpg') }}" width="80%"/>
            <div>
                <h3>Tertarik menjadi guru di courseline ?</h3>
                <a href="{{ asset('register/teacher') }}" class="btn btn-danger">Daftar Menjadi Guru</a>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('lib/jquery/jquery-2.2.4.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('lib/jwt-decode/jwt-decode.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>