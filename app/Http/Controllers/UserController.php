<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Usersdata;

class UserController extends Controller
{
    protected $table = 'users';

    protected function usersData($data){
    }

    public function store(Request $request){

        if($this->checkRecord($this->table,'email',$request->email)){
            $this->httpResponse('exist', true);
        }

        $user = new User;

        $user->email = $request->email;
        $user->password = $request->password;
        $user->first_name = $request->firstName;
        $user->last_name = $request->lastName;
        $user->roles_id = $request->roleId;
        $user->status = $request->status;

        if($user->save()){
            return $this->httpResponse('insert',true,$user);
        }

        return $this->httResponse('insert', false);
    }

    public function update(Request $request, $id){
        
        if(!$this->checkData($this->table, 'id',$id)){
            return $this->httpResponse('exist',false);
        }

        $user = User::find($id);

        $user->first_name = $request->firstName;
        $user->last_name = $request->lastName;
        $user->gender = $request->gender;
        $user->country = $request->country;
        $user->phone_number = $request->phoneNumber;
        $user->skype_id = $request->skypeId;
        $user->line_id = $request->lineId;
        $user->birthday = $request->birthday;
        $user->vision = $request->vision;
        $user->mission = $request->mission;
        $user->motto = $request->motto;

        if($request->has('image')){

            $image = $request->file('image'); $input['imagename'] =
            time().'.'.$user->id.'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $input['imagename']);
            $this->postImage->add($input);

        }

        if($user->save()){
            return $this->httpResponse('update',true,$user);
        }

        return $this->httResponse('update', false);
    }

    public function statusUser($id,$stat){

        if(!$this->checkData($this->table, 'id',$id)){
            return $this->httpResponse('exist',false);
        }

        if($stat === 'enabled'){
            $stat = 1;
        }else{
            $stat = 0;
        }

        $user = User::find($id);
        $user->status = $stat;

        if($user->save()){
            return $this->httpResponse('update',true,$user);
        }

        return $this->httResponse('update', false);
    }

    public function changePassword(Request $request,$id){

        if(!$this->checkData($this->table, 'id',$id)){
            return $this->httpResponse('exist',false);
        }

        $user = User::find($id);
        
        $user->password = $request->password; 


        if($user->save()){
            return $this->httpResponse('update',true,$user);
        }

        return $this->httResponse('update', false);
    }

}