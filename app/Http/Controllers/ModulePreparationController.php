<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModulePreparation;

class ModulePreparationController extends Controller
{

	protected $table = 'module_preparations';

	public function store(Request $request){

		$data = new ModulePreparation;

		$data->course_modules_id = $request->courseModuleId;
		$data->schedules_id = $request->scheduleId;
		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request,$id){

		$data = Feedback::find($id);

		$data->course_modules_id = $request->courseModuleId;
		$data->schedules_id = $request->scheduleId;
		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);

	}

	protected function storeModule(){
	}


	protected function destroyModule(){
	}

}
