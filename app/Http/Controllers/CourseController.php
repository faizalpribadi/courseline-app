<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;

class CourseController extends Controller
{

	protected $table = 'courses';

	public function __construct(){
	}

	public function store(Request $request){


		$data = new Course;

		$data->course_category_id = $request->courseCategory;
		$data->course_level_id = $request->courseLevel;
		$data->course_grade_id = $request->courseGrade;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$this->middleware('CheckUser');

		$data = Course::find($id);

		$data->course_category_id = $request->courseCategory;
		$data->course_level_id = $request->courseLevel;
		$data->course_grade_id = $request->courseGrade;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}
		
}
