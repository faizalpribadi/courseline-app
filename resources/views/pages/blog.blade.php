@extends('layouts.app')
@section('content')

<section class="kopa-area kopa-area-breadcrumb">
	<div class="container">
		<div class="widget kopa-widget-breadcrumb">
			<div class="widget-content">
				<h2 style="color: white;font-weight: bold;">Blog</h2>
				<div class="breadcrumb-nav">
					<span>
						<a href="#">
							<span>Home</span>
						</a>
					</span>
					<span class="current-page">Blog</span>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="kopa-area kopa-area-05">
	<div class="container">
		<div class="widget kopa-widget-news">
			<div class="widget-content module-news-05">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item  kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<article class="entry-item kopa-item-01">
							<div class="entry-thumb">
								<a href="#">
									<img src="http://placehold.it/369x224" alt="">
								</a>
							</div>
							<div class="entry-content">
								<div class="entry-date style-01">
									<span>18</span>
									<p>now</p>
								</div>
								<h4 class="entry-title">
									<a href="#">Lorem ipsum dolor sit amet.</a>
								</h4>
								<div class="entry-meta">
									<a href="#">by Admin</a>
									<a href="#">1 januar 2016</a>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodiu tempor incididunt ut labore et dolore magna aliqua.
								</p>
							</div>
						</article>
					</div>
				</div>
				<div class="kopa-pagination style-02">
					<nav class="navigation pagination">
                        <div class="nav-links">
                        	<a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a>
                        	<a class="page-numbers current" href="#">1</a>
                            <span class="page-numbers">2</span>
                            <a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a> 
                        </div>
                    </nav>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection