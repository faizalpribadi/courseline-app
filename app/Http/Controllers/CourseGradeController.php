<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseGrade;

class CourseGradeController extends Controller
{

	protected $table = 'course_grade';

	public function __construct(){
	}

	public function store(Request $request){


		$data = new CourseGrade;

		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = CourseGrade::find($id);

		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}

}
