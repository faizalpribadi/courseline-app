<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;

class ScheduleController extends Controller
{

	protected $table = 'schedules';

	public function storeStudent(Request $request){

		$data = new Schedule;

		$data->trainer_id = $request->trainerId;
		$data->users_id = $request->userId;
		$data->course_modules_id = $request->courseModuleId;
		$data->start_at = $request->start;
		$data->end_at = $request->end;

		if($request->has('trial')){
			$data->trial = $request->trial;
		}

		if($data->save()){
			return $this->httResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = Schedule::find($id);

		$data->trainer_id = $request->trainerId;
		$data->users_id = $request->userId;
		$data->course_modules_id = $request->courseModuleId;
		$data->start_at = $request->start;
		$data->end_at = $request->end;

		if($data->save()){
			return $this->httResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}

	public function cancelation(Request $request, $id){

		$schedule = Schedule::find($id);

		$schedule->status = 3;
		
		if($schedule->save()){
			$user = Auth::user();
			$data = User::find($user->id);
			$data->cancelation = $data->cancelation - 1;
	
			if($data->save()){
				return $this->httpResponse('update', true, $data);
			}
	
			return $this->httpResponse('update', false);
		}

		return $this->httpResponse('update', false);
	}

}
