<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Register | Courseline - Belajar menjadi lebih mudah</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/extra-style.css" >
</head>
<body class="disp-tbl">
<div class="wrapper">

    <a href="{{ url('/') }}" class="btn btn-back"> <i class="fa fa-arrow-left"></i><span>Back to Courseline</span></a>

    <div class="col-sm-3 col-sm-offset-3 text-center">
        <div class="box-wrapper">
            <a href="{{ url('register/student') }}" class="btn btn-warning"> Daftar menjadi murid </a>
        </div>
    </div>

    <div class="col-sm-3 text-center">
        <div class="box-wrapper">
            <a href="{{ url('register/teacher') }}" class="btn btn-danger"> Daftar menjadi guru </a>
        </div>
    </div>

</div>

<script src="lib/jquery/jquery-2.2.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</body>
</html>