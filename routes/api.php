<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user',function(){
    return response()->json(['data'=>Auth::user()],200);
})->middleware(['web','auth:api']);

Route::group(['prefix' => 'users', 'middleware' => ['web','auth:api','role:1']], function(){
    Route::get('/', 'UserController@index');
    Route::get('{id}', 'UserController@show');
    Route::delete('{id}/delete', 'UserController@destroy');
    Route::put('{id}/{status}', 'UserController@statusUser');
});

Route::group(['prefix' => 'users', 'middleware' => ['web','auth:api','role:1,2,3']], function(){
    Route::put('{id}/edit', 'UserController@update');
    Route::put('{id}/change_password', 'UserController@changePassword');
});

Route::group(['prefix' => 'oauth', 'middleware'=>'web'], function(){
    Route::post('register','UserController@store');
    Route::get('{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('{provider}/callback', 'Auth\LoginController@handleProviderCallback');
    Route::get('{provider}/token', 'Auth\LoginController@getProviderToken');
});

Route::group(['prefix' => 'course','middleware'=> 'web' ],function(){
    Route::get('/', 'CourseController@index');
    Route::get('category', 'CourseCategoryController@index');
    Route::get('category/{id}', 'CourseCategoryController@show');
    Route::get('level', 'CourseLevelController@index');
    Route::get('level/{id}', 'CourseLevelController@show');
    Route::get('module','CourseModulesController@index');
    Route::get('module/{id}','CourseModulesController@show');
    Route::get('{id}', 'CourseController@show');
});

Route::group(['prefix' => 'course','middleware'=> ['web','auth:api','role:1'] ],function(){
    Route::post('/','CourseController@store');
    Route::put('{id}/edit', 'CourseController@update');
    Route::delete('{id}/delete', 'CourseController@destroy');
    Route::post('category','CourseCategoryController@store');
    Route::put('category/{id}/edit', 'CourseCategoryController@update');
    Route::delete('category/{id}/delete', 'CourseCategoryController@destroy');
    Route::post('level','CourseLevelController@store');
    Route::put('level/{id}/edit', 'CourseLevelController@update');
    Route::delete('level/{id}/delete', 'CourseLevelController@destroy');
    Route::post('module','CourseModulesController@store');
    Route::put('module/{id}/edit', 'CourseModulesController@update');
    Route::delete('module/{id}/delete', 'CourseModulesController@destroy');
});

Route::group(['prefix'=>'module','middleware'=>'web'],function(){
    Route::get('/','ModulePreparationController@index');
    Route::get('{id}','ModulePreparationController@show');
});

Route::group(['prefix' => 'feedback','middleware'=>'web'],function(){
    Route::get('/','FeedbackController@index');
    Route::get('{id}','FeedbackController@show');
});

Route::group(['prefix'=>'notification','middleware'=>'web'],function(){
    Route::get('/', 'NotificationController@index');
    Route::get('{id}', 'NotificationController@show');
});

Route::group(['prefix' => 'role', 'middleware'=> ['web','auth:api','role:1'] ], function(){
    Route::get('/', 'RoleController@index');
    Route::get('{id}', 'RoleController@show');
    Route::post('/', 'RoleController@store');
    Route::put('{id}/edit', 'RoleController@update');
    Route::put('{id}/delete', 'RoleController@destroy');
});

Route::group(['prefix' => 'schedule', 'middleware'=> ['web','auth:api','role:1'] ], function(){
    Route::get('/', 'ScheduleController@index');
});

Route::group(['prefix' => 'schedule', 'middleware'=> ['web','auth:api','role:1'] ], function(){
    Route::get('{id}', 'ScheduleController@show');
});

Route::group(['prefix' => 'role', 'middleware'=> ['web','auth:api','role:1'] ], function(){
    Route::post('/', 'RoleController@store');
    Route::put('{id}/edit', 'RoleController@update');
    Route::put('{id}/delete', 'RoleController@destroy');
});

Route::group(['prefix' => 'transaction','middleware' => ['web','auth:api']],function(){
    Route::get('','TransactionController@index');
    Route::get('{id}','TransactionController@show');
    Route::post('/','TransactionController@store');
    Route::delete('{id}/delete','TransactionController@destroy');
});

Route::group(['prefix' => 'userdata','middleware'=>['web','auth:api','role:1'] ], function(){
    Route::get('/', 'UserDataController@index');
    Route::get('{id}', 'UserDataController@show');
    Route::post('/','UserDataController@store');
    Route::delete('{id}/delete','UserDataController@destroy');
});