<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseCategory;

class CourseCategoryController extends Controller
{

	protected $table = 'course_category';

	public function __construct(){
	}

	public function store(Request $request){


		$data = new CourseCategory;

		$data->name = $request->name;
		$data->description = $request->description;
		$data->image = $request->image;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = CourseCategory::find($id);

		$data->name = $request->name;
		$data->description = $request->description;
		$data->image = $request->image;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}


}
