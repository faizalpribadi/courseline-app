<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;

class FeedbackController extends Controller
{

	protected $table = 'feedback';

	public function store(Request $request){

		$data = new Feedback;

		$data->from = $request->from;
		$data->to = $request->to;
		$data->content = $request->content;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request,$id){

		$data = Feedback::find($id);

		$data->from = $request->from;
		$data->to = $request->to;
		$data->content = $request->content;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);

	}

}