<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimeAvailable;

class TimeAvailableController extends Controller
{

	protected $table = 'time_available';

	public function store(Request $request){

		$data = new TimeAvailable;

		$data->users_id = $request->userId;
		$data->start_at = $request->startAt;
		$data->end_at =$request->endAt;

		if($data->save()){
			return $this->httResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = TimeAvailable::find($id);

		$data->name = $request->name;
		$data->description = $request->description;

		if($data->save()){
			return $this->httResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);

	}
}
