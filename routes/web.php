<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.home');
});

Route::get('login', function () {
    return view('auth.login');
});

Route::get('about-us', function(){
	return view('pages.about-us');
});

Route::get('corporate-service',function(){
	return view('pages.corporate-service');
});
Route::get('be-teacher',function(){
	return view('pages.be-teacher');
});
Route::get('contact-us',function(){
	return view('pages.contact-us');
});

Route::get('blog', function(){
	return view('pages.blog');
});

Route::group(['prefix'=>'private-service'], function(){

	Route::get('/',function(){
		return view('pages.private-service');
	});

	Route::get('/vocaline',function(){
		return view('pages.private-service.vocaline');
	});
	Route::get('/businessline',function(){
		return view('pages.private-service.businessline');
	});
	Route::get('/languageline',function(){
		return view('pages.private-service.languageline');
	});
	Route::get('/schooline',function(){
		return view('pages.private-service.schooline');
	});

});

Route::group(['prefix'=>'register'], function(){
	Route::get('/',function(){
		return view('auth.register');
	});
	Route::get('/student',function(){
		return view('auth.register.student');
	});
	Route::get('/teacher',function(){
		return view('auth.register.teacher');
	});
});


Route::group(['prefix'=>'dashboard'],function(){
	Route::get('/', function(){
		return view('dashboard.index');
	});
});

Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);