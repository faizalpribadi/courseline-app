<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login | Courseline - Belajar menjadi lebih mudah</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/extra-style.css') }}" >

</head>
<body>


    <div class="wrapper">

        <a href="{{ url('/') }}" class="btn btn-back"> <i class="fa fa-arrow-left"></i><span>Back to Courseline</span></a>
    
    <div class="box-wrapper col-lg-3 col-md-5 col-sm-6 col-xs-8 col-centered">
            <div class="col-sm-12 col-md-7 col-lg-7 vcenter">
                <div class="row">
                    <form id="formLogin">
                    <div class="col-sm-12 text-center" style="margin-bottom: 40px;">
                        <img src="img/extra/logo.png" width="50%"/>
                        <p>Masukkan nomor handphone atau <br/> email terdaftar untuk login.</p>
                    </div>
     
                    <div class="col-sm-12">
                        <div class="form-group form-simple">
                            <label>Email / Username</label>
                            <input type="email" class="form-control" id="identify" name="identify" required/>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-simple">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" id="password" required/>
                        </div>
                    </div>

                    <div class="col-sm-12 text-right">
                        <div class="form-group form-simple">
                            <a href="#">Lupa Password ?</a>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group text-center">
                            <button class="btn btn-login form-control" id="loginBtn">Login</button>
                        </div>
                    </div>
                    </form>
                    <div class="col-sm-12">
                        <div class="text-center or-text">
                            <hr>
                            <span>ATAU</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group text-center">
                            <a href="{{ url('api/oauth/github') }}" class="btn btn-facebook form-control" id="loginFB">Login With Facebook</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="text-center">
                        Tidak memiliki akun ?<br/>
                        <a href="{{ asset('register') }}" style="text-decoration: none;">Create Account</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

<script src="{{ asset('lib/jquery/jquery-2.2.4.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('lib/jwt-decode/jwt-decode.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/login.js') }}"></script>

</body>
</html>