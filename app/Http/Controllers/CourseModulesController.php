<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseModule;

class CourseModulesController extends Controller
{

	protected $table = 'course_modules';

	public function __construct(){
	}

	public function store(Request $request){


		$data = new CourseModule;

		$data->courses_id = $request->coursesId;
		$data->name = $request->name;
		$data->description = $request->description;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = CourseLevel::find($id);

		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}
	
}