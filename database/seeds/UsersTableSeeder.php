<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create(['email' => 'hadypratamaa4@gmail.com','password' => bcrypt('password01') ]);
    }
}
