@extends('layouts.app')

@section('content')
				<section class="kopa-area kopa-area-breadcrumb" style="padding-top: 300px;background:url('assets/2.jpg');background-attachment: fixed;background-size: 100%;">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">Schooline</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span>
										<a href="#">
											<span>Private Service</span>
										</a>
									</span>
									<span class="current-page">Schooline</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget">
							<div class="widget-content">

								<div class="text-center" style="margin-bottom: 50px;">
									<h3>Apa itu SCHOOLINE ?</h3>
									<p>SCHOOLINE merupakan layanan kursus online untuk belajar pelajaran Sekolah Dasar (SD), Sekolah Menengah Pertama (SMP), dan Sekolah Menengah Atas (SMA).</p>
								</div>

								<div class="">
									<center><h3>Apa saja yang akan kamu pelajari ?</h3></center>
									<ul class="list-checkmark">
									  <li style="width: 33%;">Pelajaran Umum Sekolah Dasar</li>
									  <li style="width: 33%;">Pelajaran Umum Sekolah Menengah Pertama</li>
									  <li style="width: 33%;">Pelajaran Umum Sekolah Menengah Atas</li>
									</ul>
								</div>

								<div style="border:1px solid;padding:40px;background: #34495e;color: white">
									<h3 style="margin: 0;margin-bottom: 20px;">Apa saja yang akan kamu dapatkan ?</h3>
									<p style="color: rgba(255,255,255,.8);">1.) Pengalaman belajar yang seru dan kamu bisa belajar di mana saja dan kapan saja.</p>
									<p style="color: rgba(255,255,255,.8);">2.) Membahas pekerjaan rumah kamu, agar kamu lebih mengerti, dan kamu lebih pintar.</p>
									<p style="color: rgba(255,255,255,.8);">
										3.) Belajar untuk ulangan dan test kamu, supaya kamu dapat nilai yang bagus, sehingga kamu lebih berprestasi di sekolah kamu.
									</p>
									<p style="color: rgba(255,255,255,.8);">
										4.) Belajar untuk apa saja yang kamu butuhkan untuk keperluan sekolah kamu.
									</p>
									<p style="color: rgba(255,255,255,.8);">
										5.) Tutor-tutor pilihan yang berpengalaman dan professional akan mengajar kamu agar kamu menjadi siswa/siswi yang pintar dan berprestasi di sekolah.
									</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget">
							<div class="widget-content">

								<div class="col-sm-12 col-xs-12">
									<table id="tablepress-3" class="tablepress tablepress-id-3 dataTable no-footer" role="grid" aria-describedby="tablepress-3_info">
									<thead>
									<tr class="row-1 odd" role="row"><th class="column-1 sorting" tabindex="0" aria-controls="tablepress-3" rowspan="1" colspan="1" aria-label="DESKRIPSI: activate to sort column ascending" style="width: 65px;">DESKRIPSI</th><th class="column-2 sorting" tabindex="0" aria-controls="tablepress-3" rowspan="1" colspan="1" aria-label="TEMPAT KURSUS PADA UMUMNYA: activate to sort column ascending" style="width: 332px;">TEMPAT KURSUS PADA UMUMNYA</th><th class="column-3 sorting" tabindex="0" aria-controls="tablepress-3" rowspan="1" colspan="1" aria-label="PRIVATE (DATANG KE RUMAH): activate to sort column ascending" style="width: 282px;">PRIVATE (DATANG KE RUMAH)</th><th class="column-4 sorting" tabindex="0" aria-controls="tablepress-3" rowspan="1" colspan="1" aria-label="COURSELINE (SCHOOLINE): activate to sort column ascending" style="width: 349px;">COURSELINE (SCHOOLINE)</th></tr>
									</thead>
									<tbody class="row-hover">
									<tr class="row-2 even" role="row">
										<td class="column-1">Biaya Kursus</td><td class="column-2">Umumnya biaya kursus dihitung per bulan bukan per sesi atau pertemuan.<br>
									Biaya kursus pada tempat kursus umumnya lebih murah dari biaya kursus private. Namun kamu juga perlu perhitungkan biaya transportasi ke tempat kursus, resiko waktu dan resiko di jalan.</td><td class="column-3">Umumnya biaya kursus dihitung per bulan bukan per sesi atau pertemuan.<br>
									Biaya relatif lebih mahal dari tempat kursus pada umumnya, karena guru juga memperhitungkan biaya transportasi, resiko waktu dan resiko di jalan untuk menuju ke rumah murid.</td><td class="column-4">Biaya kursus dihitung per sesi atau pertemuan. Biaya kursus lebih murah dibandingkan dengan tempat-tempat kursus lainnya.</td>
									</tr><tr class="row-3 odd" role="row">
										<td class="column-1">Metode Belajar</td><td class="column-2">Belajar Berkelompok:<br>
									Umumnya dikelompokan berdasarkan kelas murid masing-masing.<br>
									<br>
									Belajar Private:<br>
									Private dengan gurunya, namun harganya cenderung jauh lebih mahal dibanding metode belajar berkelompok.</td><td class="column-3">Private dengan gurunya, namun harganya cenderung jauh lebih mahal karena guru juga memperhitungkan biaya transportasi, resiko waktu dan resiko di jalan untuk menuju ke rumah murid.</td><td class="column-4">Online dan private dengan gurunya dengan harga yang lebih murah dibandingkan dengan tempat-tempat kursus lain.</td>
									</tr><tr class="row-4 even" role="row">
										<td class="column-1">Guru</td><td class="column-2">1 Guru atau Lebih.</td><td class="column-3">Umumnya diajar oleh 1 guru.</td><td class="column-4">Diajar oleh banyak guru.</td>
									</tr><tr class="row-5 odd" role="row">
										<td class="column-1"></td><td class="column-2">- Ada tempat kursus yang hanya menyediakan 1 guru untuk 1 kelompok atau 1 orang (private).<br>
									- Ada juga tempat kursus yang menyediakan lebih dari 1 guru, namun untuk tempat kursus yang menyediakan lebih dari 1 guru harganya cenderung lebih mahal.</td><td class="column-3">Kalau beruntung kamu akan belajar dengan guru yang bagus, kalau tidak beruntung kamu akan belajar dengan guru yang biasa saja.<br>
									Cenderung ganti-ganti guru private dengan harga yang berbeda-beda kalau murid tidak cocok dengan gurunya.</td><td class="column-4">Kamu akan belajar dengan guru-guru yang bagus yang sudah melalui proses seleksi, bahkan kamu bisa memilih mau belajar dengan guru siapa saja, jadi kamu bisa belajar dengan guru-guru terbaik kamu.</td>
									</tr><tr class="row-6 even" role="row">
										<td class="column-1">Jam Kursus</td><td class="column-2">Ditentukan oleh tempat kursus, umumnya menyesuaikan waktu guru yang masih kosong.</td><td class="column-3">Berdasarkan kesepakatan antara guru dan murid, umumnya disesuaikan dengan waktu murid dan guru yang masih kosong.</td><td class="column-4">Sangat flexible, jam kursus ditentukan oleh kamu sendiri. Kamu bisa belajar waktu weekdays, weekend, hari libur, pagi, siang, sore, malam, atau kapan saja.</td>
									</tr><tr class="row-7 odd" role="row">
										<td class="column-1">Sesi Kursus</td><td class="column-2">Ditentukan oleh tempat kursus, umumnya menyesuaikan waktu guru yang masih kosong.</td><td class="column-3">Umumnya 2 kali atau  2 Sesi dalam 1 minggu.</td><td class="column-4">Kamu bisa mengambil sesi sesuai keinginan kamu, 1 hari 2 sesi, atau 1 minggu 7 sesi, atau yang lainnya sesuai keinginan kamu.</td>
									</tr><tr class="row-8 even" role="row">
										<td class="column-1">Tempat Kursus</td><td class="column-2">Di tempat kursus tersebut.</td><td class="column-3">Di rumah murid.</td><td class="column-4">Sangat flexible, kamu bisa belajar di mana saja. Kamu bisa belajar di rumah, kantor, sekolah, kampus, mall, kafe, atau di mana saja.</td>
									</tr><tr class="row-9 odd" role="row">
										<td class="column-1">Transportasi</td><td class="column-2">Angkot, Bus, Taxi, Ojek, Sepeda, Motor atau Mobil.</td><td class="column-3">Murid: Tidak perlu pakai transportasi apa-apa, karena belajar di rumah.<br>
									Guru: Angkot, Bus, Taxi, Ojek, Sepeda, Motor atau Mobil untuk menuju ke rumah murid, oleh karena itu biaya guru privat lebih mahal karena perlu biaya transportasi.</td><td class="column-4">Baik guru maupun murid, tidak perlu pakai transportasi apa-apa, jadi tidak perlu keluarkan duit untuk bayar bus, angkot, taxi, ojek atau untuk bayar bensin motor dan mobil.</td>
									</tr><tr class="row-10 even" role="row">
										<td class="column-1">Resiko Waktu</td><td class="column-2">Rugi waktu di jalan.</td><td class="column-3">Murid: Tidak rugi waktu di jalan, karena belajar di rumah.<br>
									Guru: Rugi di jalan, oleh karena itu biaya guru privat lebih mahal karena mereka mempertimbangkan waktu di jalan. Kalau jalannya macet, guru tidak dapat datang tepat waktu. Guru cenderung tidak mau melanjutkan mengajar karena alasan waktu.</td><td class="column-4">Baik guru maupun murid, tidak rugi waktu, karena guru dan murid dapat melakukan proses belajar mengajar di mana saja dan kapan saja.</td>
									</tr><tr class="row-11 odd" role="row">
										<td class="column-1"></td><td class="column-2">Rugi biaya kursus karena biaya kursus dihitung per bulan, jadi murid akan rugi biaya kursus kalau tidak bisa ikut kursus karena ada ekstrakurikuler, ada kerja kelompok atau pulang sekolah sudah sore.</td><td class="column-3">Rugi biaya kursus karena biaya kursus dihitung per bulan, jadi murid akan rugi biaya kursus kalau tidak bisa ikut kursus karena ada ekstrakurikuler, ada kerja kelompok atau pulang sekolah sudah sore. <br>
									Seringkali guru menunggu di rumah murid namun muridnya belum datang sehingga guru umumnya enggan menyediakan kelas pengganti. Kalaupun sudah diinfo sebelumnya belum tentu gurunya bisa menyediakan kelas pengganti karena kesibukan dan padatnya jadwal guru.</td><td class="column-4">Tidak rugi biaya kursus, karena kalau kamu tidak bisa ikut kursus, kamu bisa melakukan reschedule. Namun karena kami ingin kamu menjadi orang yang disiplin, waktu reschedule kami batasi.</td>
									</tr><tr class="row-12 even" role="row">
										<td class="column-1"></td><td class="column-2">Kalau murid berhalangan untuk mengikuti kursus. Untuk metode belajar kelompok, tempat kursus tidak akan memberikan kelas pengganti. Untuk metode belajar private, ada tempat kursus yang bersedia menyediakan tempat pengganti namun pada umumnya tempat kursus cenderung tidak menyediakan kelas pengganti.</td><td class="column-3">Kalau murid berhalangan untuk mengikuti kursus,  guru private cenderung belum tentu bisa menyediakan kelas pengganti karena kesibukan dan padatnya jadwal guru. Kalau pun bisa, waktu untuk kelas pengganti harus disesuaikan dengan waktu murid dan gurunya.</td><td class="column-4">Kamu bisa melakukan reschedule untuk waktu kursus kapan saja, belajar di mana saja, dan bisa pilih mau belajar dengan guru siapa saja.</td>
									</tr><tr class="row-13 odd" role="row">
										<td class="column-1">Resiko Di Jalan</td><td class="column-2">Jauh ke tempat kursusnya.</td><td class="column-3">Murid: Tidak perlu pergi, karena gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: Jaraknya jauh untuk menuju rumah murid. Cenderung kelelahan di jalan dan cenderung akan berhenti mengajar.</td><td class="column-4">Baik guru maupun murid, tidak perlu pergi ke tempat kursus, karena bisa melakukan proses belajar dan mengajar di mana saja.</td>
									</tr><tr class="row-14 even" role="row">
										<td class="column-1"></td><td class="column-2">Macet dan kelelahan di jalan.</td><td class="column-3">Murid: Tidak merasakan macet dan kelelahan, karena gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: Macet dan kelelahan di jalan, sehingga telat sampai di rumah murid dan kurang konsentrasi dalam mengajar.</td><td class="column-4">Baik guru maupun murid, tidak merasakan macet dan tidak akan kelelahan di jalan, sehingga proses belajar mengajar berlangsung dengan baik, efektif dan kondusif.</td>
									</tr><tr class="row-15 odd" role="row">
										<td class="column-1"></td><td class="column-2">Hujan, becek, lupa bawa payung dan basah.</td><td class="column-3">Murid: Tidak kehujanan atau kebasahan, karena gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: Hujan, becek, lupa bawa payung dan basah.</td><td class="column-4">Baik guru maupun murid, tidak akan kehujanan atau kebasahan di jalan, sehingga proses belajar mengajar berlangsung dengan baik, efektif dan kondusif.</td>
									</tr><tr class="row-16 even" role="row">
										<td class="column-1"></td><td class="column-2">Dicopet waktu di jalan menuju tempat kursus.</td><td class="column-3">Murid: Tidak akan kecopetan di jalan karena gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: Dicopet waktu di jalan menuju ke rumah murid, sehingga kemungkinan guru sulit berkonsentrasi dalam mengajar karena memikirkan barang-barangnya yang dicopet.</td><td class="column-4">Baik guru maupun murid, tidak akan kecopetan waktu di jalan menuju tempat kursus, karena tempat kursusnya bisa di mana saja, di rumah, kantor, dan di tempat yang kamu rasa jadi tempat yang paling aman, sehingga proses belajar mengajar berlangsung dengan baik, efektif dan kondusif. </td>
									</tr><tr class="row-17 odd" role="row">
										<td class="column-1"></td><td class="column-2">Kemungkinan terjadinya kecelakaan waktu di jalan menuju tempat kursus.</td><td class="column-3">Murid: Gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: kemungkinan terjadinya kecelakaan waktu di jalan menuju rumah murid, sehingga tidak memungkinkan untuk mengajar. Kalaupun dipaksakan, guru sulit berkonsentrasi untuk mengajar.</td><td class="column-4">Baik guru maupun murid, kemungkinan terjadi kecelakaan waktu di jalan menuju tempat kursus sangat kecil atau bahkan tidak ada, karena tempat kursusnya bisa di mana saja, di rumah, kantor, dan di tempat yang kamu rasa jadi tempat yang paling aman, sehingga proses belajar mengajar berlangsung dengan baik, efektif dan kondusif.</td>
									</tr><tr class="row-18 even" role="row">
										<td class="column-1"></td><td class="column-2">Anak: Penculikan anak yang mungkin terjadi di jalan.</td><td class="column-3">Murid: Tidak ada resiko penculikan anak, karena gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru: Mungkin tidak ada penculikan namun ada beberapa resiko di jalan yang mungkin akan dihadapi.</td><td class="column-4">Anak-anak tidak perlu keluar rumah untuk kursus, jadi resiko penculikan anak sangat kecil atau bahkan tidak ada, karena anak-anak bisa belajar di mana saja di tempat yang paling aman bagi mereka seperti di rumah atau sekolah.</td>
									</tr><tr class="row-19 odd" role="row">
										<td class="column-1"></td><td class="column-2">Wanita dan anak-anak: Pelecehan seksual yang mungkin terjadi di jalan.</td><td class="column-3">Murid: Gurunya yang akan datang ke rumahnya.<br>
									<br>
									Guru Wanita: Pelecehan seksual yang mungkin terjadi di jalan menuju rumah murid.</td><td class="column-4">Baik guru wanita atau anak-anak, tidak usah takut tertimpa kasus pelecehan seksual dalam perjalanan ke tempat kursus, karena kamu bisa belajar di mana saja yang menurut kamu menjadi tempat yang paling aman untuk kursus.</td>
									</tr></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
@endsection