@extends('layouts.app')

@section('style')
@endsection

@section('content')

				<section class="kopa-area kopa-area-breadcrumb" style="padding-top: 300px;background:url('assets/2.jpg');background-attachment: fixed;background-size: 100%;">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">About Us</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span class="current-page">About us</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget-course_info">
							<div class="widget-content module-course_info-02">
								<div class="row">
									<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
										<ul class="nav nav-tabs" role="tablist">
										    <li class="active">
										    	<a href="#t1" aria-controls="t1" role="tab" data-toggle="tab">
										    		ABOUT COURSELINE
										    	</a>
										    </li>
										    <li>
										    	<a href="#t2" aria-controls="t2" role="tab" data-toggle="tab"s style="text-transform: uppercase;font-weight: bold;">
										    		Courseline Philosophy
										    	</a>
										    </li>
										</ul>
									</div>
									<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
										<div class="tab-content">
											<div class="tab-pane fade in active" id="t1">
												<div class="course-author">
													<div class="content">
														<h4>About Courseline</h4>
														<span>Tentang Courseline</span>
														<div class="text">
															<i class="fa fa-quote-left"></i>
															<p>COURSELINE merupakan sebuah one-stop online learning center yang memadukan program kursus dan teknologi, di mana pengajar dan murid dapat melakukan program kursus ini di mana saja dan kapan saja (on line course based). Keberadaan COURSELINE diharapkan dapat membantu setiap orang untuk mengembangkan bakat, potensi, intelegensi dan kemampuannya masing-masing tanpa ada kendala jarak dan waktu, sehingga COURSELINER (Peserta Didik Courseline) dapat bekerja sesuai dengan passion dan COURSELINER dapat berhasil dan dapat mencapai impiannya.</p>
															<p>COURSELINE memiliki pengajar-pengajar yang berpengalaman, baik pengajar lokal maupun dari luar, yang dapat mengembangkan bakat, potensi dan kemampuan setiap orang yang memiliki keinginan yang besar untuk menjadi seorang profesional yang berhasil. Ditambah dengan kurikulum kursus yang komprehensif, integrative, dan terstruktur dapat mempermudah setiap orang yang mencapai impiannya untuk menjadi seorang professional yang berhasil.</p>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="t2">
												<div class="course-author">
													<div class="content">
														<h4>Philosophy Courseline</h4>
														<span>Tentang Courseline</span>
														<div class="text">
															<i class="fa fa-quote-left"></i>
															<p>Setiap orang yang terlahir di dunia pasti dianugerahi bakat (kemampuan dasar atau potensi) oleh Sang Pencipta, Tuhan yang Maha Esa. Ada dua jenis bakat yang dimiliki setiap orang, yaitu bakat umum dan bakat khusus. Bakat umum merupakan kemampuan berupa potensi dasar yang bersifat umum atau yang dimiliki setiap orang seperti berbicara dan berkendara, sedangkan bakat khusus (talent) merupakan kemampuan berupa potensi khusus yang tidak semua orang memilikinya seperti berbahasa, bermusik, mekanik, olahraga, dan bakat khusus lainnya.
																											</p>

																											<p>
															Setiap orang akan bekerja dengan sepenuh hati apabila pekerjaannya sesuai dengan passionnya dan passion ini cenderung berkaitan dengan bakat orang tersebut. Misalnya seseorang memiliki bakat bernyanyi, orang tersebut akan cenderung bekerja dengan sepenuh hati apabila dia bekerja sebagai penyanyi professional dibanding bekerja sebagai karyawan administrasi. Kemudian seseorang yang memiliki bakat berbahasa akan cenderung bekerja sepenuh hati apabila dia bekerja sebagai guru bahasa atau penterjemah dibanding bekerja sebagai akuntan.
															</p>

															<p>Hidup yang berhasil merupakan dambaan dan impian dari setiap orang. Setiap orang sebenarnya memiliki kesempatan untuk menjadi orang yang berhasil di dalam hidupnya. Dan ada beberapa hal yang mempengaruhi keberhasilan seseorang di antaranya adalah lingkungan, pengalaman, dan akademik. Namun keberhasilan tersebut sangat bergantung pada dirinya sendiri. Perbedaan orang berhasil dan orang yang gagal hanyalah kemauan. Orang yang mau terus belajar dan mengembangkan dirinya cenderung akan menjadi orang yang berhasil, namun orang yang enggan belajar dan mengembangkan dirinya cenderung akan menjadi orang yang biasa-biasa saja, tidak mengalami peningkatan, atau bahkan menjadi orang yang gagal.</p>

															<p>Di era digital saat ini, seperti yang sudah kita ketahui dan rasakan, teknologi dan sistem informasi berkembang dengan sangat pesat. Semua yang tidak mungkin di masa lalu, menjadi sangat mungkin di era digital saat ini. Dari mulai ditemukan telegram, telepon, kemudian classic handphone yang hanya dapat memberikan pesan singkat dan menelepon, dan berkembang menjadi smartphone yang tidak hanya dapat memberikan pesan singkat dan menelepon, tetapi dapat mengirimkan gambar, video, dan bahkan dapat melakukan komunikasi tatap muka dua arah melalui video call melalui beberapa media seperti smartphone dan laptop.</p>

															<p>Ada tiga masalah yang dialami oleh banyak orang di zaman ini:</p>

															<ol type="1">
																<li style="margin:20px 0;">Banyak orang saat ini yang bekerja tidak sesuai dengan passionnya atau tidak bekerja sesuai dengan bakat yang dimilikinya.</li>

																<li style="margin:20px 0;">Banyak orang yang ingin mengembangkan bakatnya namun jarak dan waktu menjadi kendala bagi mereka.</li>

																<li style="margin:20px 0;">Di era digital saat ini sudah sangat banyak orang-orang yang dapat menggunakan internet, namun banyak dari mereka belum memanfaatkan internet dan teknologi untuk mengembangkan bakat mereka. Ketiga hal tersebut dialami oleh setiap orang di dunia, dan hal tersebut menjadi masalah tingkat dunia yang perlu diatasi dan ditangani.</li>

															</ol>

															<p>Melalui metode belajar mengajar yang dipadukan dengan teknologi yang sudah sangat canggih di era digital ini, COURSELINE hadir dengan tujuan untuk menjadi solusi untuk ketiga masalah tersebut. Setiap orang dapat menggunakan teknologi yang sangat canggih ini untuk mengembangkan bakatnya tanpa ada kendala jarak dan waktu, sehingga mereka dapat bekerja sesuai dengan bakat dan passion mereka, dan mereka dapat berhasil dan mencapai impian mereka.</p>

															<p>COURSELINE memadukan dua hal yang sangat luar biasa ini, yaitu COURSE & ONLINE. Dengan pengajar yang sangat berpengalaman, baik pengajar lokal maupun dari luar nantinya, dapat mengasah potensi setiap orang yang memiliki keinginan yang besar untuk menjadi seorang profesional yang berhasil. Kemudian kurikulum yang komprehensif, integrative, dan terstruktur dapat mempermudah setiap orang yang mencapai impian mereka untuk menjadi seorang profesional yang berhasil. Dengan memanfaatkan kecanggihan teknologi, pengajar dan murid dapat melakukan proses kursus di mana saja dan kapan saja, dan tentunya akan mempermudah setiap orang untuk mencapai impiannya menjadi seseorang yang professional dan berhasil.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area kopa-area-19 white-text-style" style="border:1px solid;background:url(' {{ asset('img/bg1.jpg') }}');background-size: 100%;background-attachment: fixed;">
					<div class="container">
						<div class="row">

							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="widget kopa-widget-mission">
									<h3 class="widget-title style-05">Vission</h3>
									<div class="widget-content module-mission-01">
										<p>Membangun dan mengembangkan bakat, potensi, kemampuan serta intelegensi miliaran orang di Asia terutama jutaan orang di Indonesia tanpa ada kendala jarak dan waktu melalui metode pembelajaran online sehingga mereka dapat menjadi professional-professional yang berhasil dalam pekerjaan yang sesuai dengan bakat, passion dan impian mereka.</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="widget kopa-widget-mission">
									<h3 class="widget-title style-05">Mission</h3>
									<div class="widget-content module-mission-01">
										<ul style="margin-top: 0;">
											<li>
												<i class="fa fa-check-circle"></i>
												Berkontribusi pada pembangunan manusia di Asia, terutama di Indonesia sehingga menjadi professional yang berkualitas.
											</li>
											<li>
												<i class="fa fa-check-circle"></i>
												Meningkatkan skill dan knowledge setiap orang di Indonesia dan Asia melalui metode pembelajaran tatap muka on-line, dengan kurikulum yang berkualitas, komprehensif dan sistematik.
											</li>
											<li>
												<i class="fa fa-check-circle"></i>
												Memberikan pengajaran kepada setiap orang di Indonesia dan Asia mengenai mental dan karakter seorang professional melalui pengajaran yang diberikan oleh para pengajar yang berpengalaman dan professional.
											</li>
										</ul>

									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

@endsection