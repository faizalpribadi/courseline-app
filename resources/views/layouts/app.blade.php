<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Courseline - Belajar menjadi lebih mudah</title>
		
		<!-- Font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700|Roboto+Slab:300,400,700" rel="stylesheet">

		<!-- Css Plugin -->
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/themify-icons/css/themify-icons.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/owl/css/owl.carousel.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/owl/css/owl.theme.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/owl/css/owl.transitions.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/slick/css/slick.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/slider-pro/css/slider-pro.min.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/jqueryCustomScrollbar/css/jquery.mCustomScrollbar.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/animate/animate.css') }}">

		<!-- My CSS -->
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/extra-style.css') }}">
		
		@yield('style')

		<!-- View Responsive -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

	</head>
	<body>
		<div id="main-container">
			<header class="kopa-header-01">
				<div class="container">
					<div class="top-header">
						<div class="kopa-search-form">
							<input type="text" name="search" placeholder="SEARCH" class="search-input">
						</div>
						<div class="kopa-login">
							<a href="{{ url('login') }}">
								<i class="fa fa-sign-in"></i>
								login
							</a>
						</div>
						<div class="kopa-register">
							<a href="{{ url('register') }}">
								<i class="fa fa-sign-in"></i>
								Register
							</a>
						</div>
					</div>
					<div class="bottom-header style-01">
						<div class="clearfix">
							<div class="pull-left">
								<h1 class="kopa-logo">
									<a href="{{ url('/') }}">
										<img src="{{ asset('img/logo.png') }}">
									</a>
								</h1>
							</div>
							<div class="pull-right">
								<nav class="kopa-main-menu style-01">
									<ul class="navbar-menu">
										<li class="menu-item-has-children current-menu-item">
											<a href="{{ url('/') }}">Home</a>
										</li>
										<li class="menu-item-has-children">
											<a href="{{ url('about-us') }}">About Us</a>
										</li>
										<li class="menu-item-has-children">
											<a href="#">Private Service</a>
											<ul style="margin-top: -10px;">
												<li><a href="{{ url('private-service/vocaline') }}">Vocaline</a></li>
												<li><a href="{{ url('private-service/languageline') }}">Languageline</a></li>
												<li><a href="{{ url('private-service/businessline') }}">Businessline</a></li>
												<li><a href="{{ url('private-service/schooline') }}">Schooline</a></li>
											</ul>
										</li>
										<li class="menu-item-has-children">
											<a href="{{ url('corporate-service') }}">Corporate Service</a>
										</li>
										<li class="border-bold">
											<a href="{{ url('become-a-teacher') }}">become a teacher</a>
										</li>
										<li class="menu-item-has-children">
											<a href="{{ url('contact-us') }}">Contact Us</a>
										</li>
										<li class="menu-item-has-children">
											<a href="{{ url('blog') }}">Blog</a>
										</li>
									</ul>
								</nav>
								<div class="kopa-hamburger-menu">
									<i class="fa fa-bars"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div id="kopa-menu-scroll">
		        <div class="kopa-close-menu-scroll">
		            <span>close x</span>
		        </div>
		        <div class="top-menu-scroll">
		        	<div class="kopa-social-links style-03">
					    <ul class="clearfix">
					        <li><span>Social</span></li>
					        <li><a href="#" class="fa fa-twitter"></a></li>
					        <li><a href="#" class="fa fa-google-plus"></a></li>
					        <li><a href="#" class="fa fa-rss"></a></li>
					    </ul>
					</div>
			        <ul class="kopa-menu-click">
			            <li>
			            	<a href="index.html">Home</a>
			            </li>
			            <li>
			            	<a href="about-us.html">About us</a>
			            </li>
			            <li class="menu-item-has-children">
							<a href="#">Private Service</a>
							<ul>
								<li><a href="vocaline.html">Vocaline</a></li>
								<li><a href="businessline.html">Businessline</a></li>
								<li><a href="languageline.html">Languageline</a></li>
								<li><a href="schooline.html">Schooline</a></li>
							</ul>
						</li>
			            <li>
							<a href="corporate-service.html">Corporate Service</a>
						</li>
			            <li>
							<a href="menjadi-pengajar.html">Menjadi Pengajar</a>
						</li>
			            <li>
							<a href="contact-us.html">Contact Us</a>
						</li>
			            <li>
							<a href="blog.html">Blog</a>
						</li>
			        </ul>
		        </div>
		        <form>
		        	<button type="submit" class="btn"><i class="fa fa-search"></i></button>
		            <input type="text" placeholder="Search courses">
		        </form>
		        <div class="widget kopa-widget-banner">
		        	<div class="widget-content module-banner-01">
		        		<ul class="kopa-banner">
				        	<li>
				        		<span>100%</span>
				        		<div>
				        			<h4>MONEY BACK GUARANTEE</h4>
				        			<p>No hassles, no question asked!</p>
				        		</div>
				        	</li>
				        	<li>
				        		<h4>EARN UP TO <span>35%</span></h4>
				        		<p>Sign up our affiliate program</p>
				        	</li>
				        </ul>
		        	</div>
		        </div>
		    </div>
			<div id="main-content">

				@yield('content')

			</div>
			<div class="bottom-sidebar style-01 white-text-style">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12 col-md-push-4">
							<div class="widget kopa-widget-logof">
								<div class="widget-content">
									<a href="#"><img class="footer-img" src="img/extra/logo-white.png" alt=""></a>
									<div class="kopa-social-links style-02">
									    <ul class="clearfix">
									        <li><a href="#" class="fa fa-facebook"></a></li>
									        <li><a href="#" class="fa fa-twitter"></a></li>
									        <li><a href="#" class="fa fa-google-plus"></a></li>
									        <li><a href="#" class="fa fa-rss"></a></li>
									    </ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 col-md-pull-4">
							<div class="widget kopa-widget-tweets">
								<h3 class="widget-title style-02">RECENT TWEETS</h3>
								<div class="widget-content module-tweets-01">
									<ul>
										<li>
											<i class="fa fa-twitter"></i>
											<div class="tweets-detail">
												<a href="#">
													<span>@ASCETICDESIGNERS,</span>
													Next Jobs Fair Will Be Held in Our Campus.
												</a>
												<span>5 Mins Ago</span>
											</div>
										</li>
										<li>
											<i class="fa fa-twitter"></i>
											<div class="tweets-detail">
												<a href="#">
													<span>@ASHIFULPAPPU,</span>
													Build Your Career at Our Paathshaala Faculties.
												</a>
												<span>25 Mins Ago</span>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="widget kopa-widget-useful">
								<h3 class="widget-title style-03">useful links</h3>
								<div class="widget-content">
									<ul>
										<li><a href="#">All Our Course</a></li>
										<li><a href="#"> Private Service </a></li>
										<li><a href="#">Corporate Service</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Terms &amp; Conditions</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="kopa-footer style-01">
				<div class="container">
					<p>© 2018 <a href="#">Digikom</a>  |  All Rights Reserved</p>
				</div>
			</footer>
		</div>

		<script src="{{ asset('lib/jquery/jquery-2.2.4.min.js') }}"></script>
		<script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('lib/slick/js/slick.min.js') }}"></script>
		<script src="{{ asset('lib/slider-pro/js/jquery.sliderPro.min.js') }}"></script>
		<script src="{{ asset('lib/images-loaded/imagesloaded.pkgd.min.js') }}"></script>
		<script src="{{ asset('lib/jquery/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('lib/owl/js/owl.carousel.min.js') }}"></script>
		<script src="{{ asset('lib/jquery/jquery.waypoints.min.js') }}"></script>
		<script src="{{ asset('lib/jquery/jquery.counterup.min.js') }}"></script>
		<script src="{{ asset('lib/jqueryCustomScrollbar/js/jquery.mCustomScrollbar.min.js') }}"></script>
		<script src="{{ asset('lib/viewportchecker/viewportchecker.min.js') }}"></script>

		@yield('scripts')

</body>
</html>