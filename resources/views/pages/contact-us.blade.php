@extends('layouts.app')
@section('content')
				<section class="kopa-area kopa-area-breadcrumb">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">Contact Us</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span class="current-page">Contact Us</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="kopa-contact-form">
							<h2>Send us a message now!</h2>
							<div role="form" class="wpcf7" lang="en-US" dir="ltr">

								<form method="post" class="wpcf7-form" novalidate="novalidate">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
												<label>
												    <span class=" your-name">
												    	<input type="text" name="your-name" aria-required="true" aria-invalid="false" placeholder="Your Name *">
												    </span> 
												</label>
											</p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
												<label>
												    <span class=" your-email">
												    	<input type="email" name="your-email" aria-required="true" aria-invalid="false" placeholder="Your E-mail *">
												    </span> 
												</label>
											</p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
												<label>
												    <span class="your-subject">
												    	<input type="text" name="your-subject" aria-invalid="false" placeholder="Your Subject *">
												    </span>
												</label>
											</p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
												<label>
												    <span class="your-phone">
												    	<input type="text" name="your-phone" aria-invalid="false" placeholder="Your Phone No.">
												    </span>
												</label>
											</p>
										</div>
										<div class="col-xs-12">
											<p>
												<label>
												    <span class="your-message">
													    <textarea name="your-message" cols="40" rows="10" aria-invalid="false" placeholder="Your Message..."></textarea>
													</span> 
												</label>
											</p>
										</div>
										<div class="col-xs-12">
											<p>
												<input type="submit" value="send message">
											</p>
										</div>
									</div>
									
									
								</form>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area kopa-area-nospace white-text-style">
					<div class="widget kopa-widget-contact_map">
						<div class="widget-content module-contact_map-01">
							<div class="container">
								<div class="contact-detail">
									<h3>Get in touch with us now!</h3>
									<div class="row">
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="detail">
												<i class="fa fa-map-marker"></i>
												<div class="content">
													<p>PO Box 21177, Little Lonsdale,</p>
													<p>Melbourne 8011, Australia</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="detail">
												<i class="fa fa-phone"></i>
												<div class="content">
													<p>+ 880 1719 45 75 93</p>
													<p>+ 880 1193 40 26 65</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="detail">
												<i class="fa fa-envelope"></i>
												<div class="content">
													<a href="#">results@paathshaala.edu</a>
													<a href="#">admission@paathshaala.edu</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kopa-map-01"></div>
						</div>
					</div>
				</section>
@endsection