@extends('layouts.app')

@section('content')
				<section class="kopa-area kopa-area-breadcrumb">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">Vocaline</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span>
										<a href="#">
											<span>Private Service</span>
										</a>
									</span>
									<span class="current-page">Vocaline</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget">
							<div class="widget-content">

								<div class="text-center" style="margin-bottom: 50px;">
									<h3>Apa itu VOCALINE ?</h3>
									<p>VOCALINE merupakan layanan kursus online untuk belajar vocal/bernyanyi.</p>
								</div>

								<div class="">
									<center><h3>Apa saja yang akan kamu pelajari ?</h3></center>
									<ul class="list-checkmark">
									  <li>Voice-Building</li>
									  <li>Warm-Ups</li>
									  <li>Postur Netral Tubuh</li>
									  <li>Postur Netral Kepala</li>
									  <li>Teknik Pernapasan</li>
									  <li>Diksi/Artikulasi</li>
									  <li>Intonasi</li>
									  <li>Dinamika</li>
									  <li>Vibrato</li>
									  <li>Run/Scales</li>
									  <li>Long Tones</li>
									  <li>Range Extension</li>
									  <li>Pharsering</li>
									  <li>Resonance</li>
									  <li>Arpeggio</li>
									  <li>Staccato</li>
									  <li>Register Vocal</li>
									  <li>Belting Voice</li>
									  <li>Sustaining Power</li>
									  <li>Agility</li>
									  <li>Yodeling</li>
									  <li>Singing Harmony</li>
									  <li>Pertunjukan</li>
									  <li>Praktek Lengkap</li>
									  <li>Vocal Care</li>
									</ul>
								</div>

								<div style="border:1px solid;padding:40px;background: #34495e;color: white">
									<h3 style="margin: 0;margin-bottom: 20px;">Apa saja yang akan kamu dapatkan ?</h3>
									<p style="color: rgba(255,255,255,.8);">1.) Pengalaman belajar yang seru dan kamu bisa belajar vocal di mana saja dan kapan saja.</p>
									<p style="color: rgba(255,255,255,.8);">2.) Guru-guru vocal pilihan yang berpengalaman dan professional akan mengajar dan melatih kamu banyak hal agar kamu dapat menjadi penyanyi professional, bahkan kamu bisa menjadi seorang bintang.</p>
									<p style="color: rgba(255,255,255,.8);">3.) File yang berisi materi-materi vocal yang akan kamu pelajari untuk meningkatkan kualitas vocal dan bernyanyi kamu.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget">
							<div class="widget-content">

								<div class="col-sm-12 col-xs-12">
									<table id="tablepress-2" class="tablepress tablepress-id-2 dataTable no-footer" role="grid" aria-describedby="tablepress-2_info">
									<thead>
									<tr class="row-1 odd" role="row"><th class="column-1 sorting" tabindex="0" aria-controls="tablepress-2" rowspan="1" colspan="1" aria-label="Deskripsi: activate to sort column ascending" style="width: 66px;">Deskripsi</th><th class="column-2 sorting" tabindex="0" aria-controls="tablepress-2" rowspan="1" colspan="1" aria-label="TEMPAT KURSUS VOCAL PADA UMUMNYA: activate to sort column ascending" style="width: 446px;">TEMPAT KURSUS VOCAL PADA UMUMNYA</th><th class="column-3 sorting" tabindex="0" aria-controls="tablepress-2" rowspan="1" colspan="1" aria-label="COURSELINE (VOCALINE): activate to sort column ascending" style="width: 544px;">COURSELINE (VOCALINE)</th></tr>
									</thead>
									<tbody class="row-hover">
									<tr class="row-2 even" role="row">
										<td class="column-1">Guru</td><td class="column-2">Umumnya diajar oleh 1 guru.</td><td class="column-3">Diajar oleh banyak guru.</td>
									</tr><tr class="row-3 odd" role="row">
										<td class="column-1"></td><td class="column-2">Kalau beruntung kamu akan belajar dengan guru yang bagus, kalau tidak beruntung kamu akan belajar dengan guru yang biasa saja.</td><td class="column-3">Kamu akan belajar dengan guru-guru yang bagus yang sudah melalui proses seleksi, bahkan kamu bisa memilih mau belajar dengan guru siapa saja, jadi kamu bisa belajar dengan guru-guru terbaik kamu.</td>
									</tr><tr class="row-4 even" role="row">
										<td class="column-1">Jam Kursus</td><td class="column-2">Ditentukan oleh tempat kursus, umumnya menyesuaikan waktu guru yang masih kosong.</td><td class="column-3">Sangat flexible, jam kursus ditentukan oleh kamu sendiri. Kamu bisa belajar waktu weekdays, weekend, hari libur, pagi, siang, sore, malam, atau kapan saja.</td>
									</tr><tr class="row-5 odd" role="row">
										<td class="column-1">Sesi Kursus</td><td class="column-2">1 Minggu - 1 Kali - 1 Sesi.</td><td class="column-3">Kamu bisa mengambil sesi sesuai keinginan kamu, 1 hari 2 sesi, atau 1 minggu 7 sesi, atau yang lainnya sesuai keinginan kamu.</td>
									</tr><tr class="row-6 even" role="row">
										<td class="column-1">Penyelesaian 1 Grade</td><td class="column-2">Rata-rata diperlukan waktu 6 bulan bahkan lebih untuk menyelesaikan 1 grade.</td><td class="column-3">Kamu bisa menyelesaikan 1 grade dalam 12 hari, 24 hari, 1 bulan, 3 bulan atau maksimal 6 bulan, semua tergantung kamu dan semangatmu.</td>
									</tr><tr class="row-7 odd" role="row">
										<td class="column-1">Tempat Kursus</td><td class="column-2">Di tempat kursus tersebut.</td><td class="column-3">Sangat flexible, kamu bisa belajar di mana saja. Kamu bisa belajar di rumah, kantor, sekolah, kampus, mall, kafe, atau di mana saja.</td>
									</tr><tr class="row-8 even" role="row">
										<td class="column-1">Transportasi</td><td class="column-2">Angkot, Bus, Taxi, Ojek, Sepeda, Motor atau Mobil.</td><td class="column-3">Tidak perlu pakai transportasi apa-apa, jadi tidak perlu keluarkan duit untuk bayar bus, angkot, taxi, ojek atau untuk bayar bensin motor dan mobil.</td>
									</tr><tr class="row-9 odd" role="row">
										<td class="column-1">Resiko Waktu</td><td class="column-2">Rugi waktu di jalan.</td><td class="column-3">Tidak rugi waktu, karena kamu bisa belajar di mana saja dan kapan saja.</td>
									</tr><tr class="row-10 even" role="row">
										<td class="column-1"></td><td class="column-2">Rugi biaya kursus karena dianggap absent dan mengurangi credit yang sudah dibayar padahal tidak bisa ikut kursus karena hal-hal berikut:<br>
									- Orang Kantoran &amp; Entrepreneurs: ada meeting, lembur atau lagi ada banyak kerjaan, resiko di jalan, dll.<br>
									- Anak Sekolah &amp; Mahasiswa: ada ekstrakurikuler, ada kerja kelompok atau pulang sekolah sudah sore, resiko di jalan.</td><td class="column-3">Tidak rugi biaya kursus, karena kalau kamu tidak bisa ikut kursus, kamu bisa melakukan reschedule. Namun karena kami ingin kamu menjadi orang yang disiplin, waktu reschedule kami batasi.</td>
									</tr><tr class="row-11 odd" role="row">
										<td class="column-1"></td><td class="column-2">Kalau tidak bisa hadir kursus, ada tempat kursus yang tidak memberikan kelas pengganti, ada juga yang menyediakan kelas pengganti tetapi harus menyesuaikan waktu lagi dengan gurunya.</td><td class="column-3">Kamu bisa melakukan reschedule untuk waktu kursus kapan saja, belajar di mana saja, dan bisa pilih mau belajar dengan guru siapa saja.</td>
									</tr><tr class="row-12 even" role="row">
										<td class="column-1">Resiko Di Jalan</td><td class="column-2">Jauh ke tempat kursusnya.</td><td class="column-3">Kamu tidak perlu pergi ke tempat kursus, bisa belajar di mana saja.</td>
									</tr><tr class="row-13 odd" role="row">
										<td class="column-1"></td><td class="column-2">Macet dan kelelahan di jalan.</td><td class="column-3">Tidak pakai macet dan tidak akan kelelahan di jalan.</td>
									</tr><tr class="row-14 even" role="row">
										<td class="column-1"></td><td class="column-2">Hujan, becek, lupa bawa payung dan basah.</td><td class="column-3">Tidak akan kehujanan apalagi kebasahan.</td>
									</tr><tr class="row-15 odd" role="row">
										<td class="column-1"></td><td class="column-2">Dicopet waktu di jalan menuju tempat kursus.</td><td class="column-3">Tidak akan kecopetan waktu di jalan menuju tempat kursus, karena tempat kursusnya bisa di mana saja, di rumah, kantor, dan di tempat yang kamu rasa jadi tempat yang paling aman.</td>
									</tr><tr class="row-16 even" role="row">
										<td class="column-1"></td><td class="column-2">Kemungkinan terjadinya kecelakaan waktu di jalan menuju tempat kursus.</td><td class="column-3">Kemungkinan terjadi kecelakaan waktu di jalan menuju tempat kursus sangat kecil atau bahkan tidak ada, karena tempat kursusnya bisa di mana saja, di rumah, kantor, dan di tempat yang kamu rasa jadi tempat yang paling aman.</td>
									</tr><tr class="row-17 odd" role="row">
										<td class="column-1"></td><td class="column-2">Anak: Penculikan anak yang mungkin terjadi di jalan.</td><td class="column-3">Anak-anak tidak perlu keluar rumah untuk kursus, jadi resiko penculikan anak sangat kecil atau bahkan tidak ada, karena anak-anak bisa belajar di mana saja di tempat yang paling aman bagi mereka seperti di rumah atau sekolah.</td>
									</tr><tr class="row-18 even" role="row">
										<td class="column-1"></td><td class="column-2">Wanita dan anak-anak: Pelecehan seksual yang mungkin terjadi di jalan.</td><td class="column-3">Untuk wanita atau anak-anak, tidak usah takut tertimpa kasus pelecehan seksual dalam perjalanan ke tempat kursus, karena kamu bisa belajar di mana saja yang menurut kamu menjadi tempat yang paling aman untuk kursus.</td>
									</tr></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
@endsection