<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{

	protected $table = 'roles';

	public function store(Request $request){

		$data = new Role;

		$data->name = $request->name;
		$data->description = $request->description;

		if($data->save()){
			return $this->httResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = Role::find($id);

		$data->name = $request->name;
		$data->description = $request->description;

		if($data->save()){
			return $this->httResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);

	}

}