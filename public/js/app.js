class App {

	constructor(){
	}

	logout(){
		localStorage.clear()
		window.location.href  = "/"
	}

	checkToken(){
        var token = localStorage.getItem('access_token');
        if(token){
			try
			{
				if (this.checkUser(token)){
					throw true
				}else{
					throw false
				}
			}
			catch(exception)
			{
				return exception;
			}
		}
		return false
	}

	checkUser(token){
		let resp;

		return new Promise((resolve, reject) => {
			$.ajax({
				type : "GET",
				headers : {
					"Content-Type" : "application/json",
					"Authorization" : "Bearer "+token
				},
				url : "api/user",
				success : function(response){
					return resolve(response);
				},
				error : function(err){
					reject(err);
				}
			})
		})
	}

	toDashboard(){
		var token = localStorage.getItem('access_token');
		this.checkUser(token).then(response => {
			switch(response['data']['roles_id']){
				case 1:
					window.location.href = "dashboard";
					break;
				case 2:
					window.location.href = "dashboard/teacher";
					break;
				case 3:
					window.location.href = "dashboard/student";
					break;
			}
		})
	}
	
	inputInteractive(){
		$('.form-simple input').focus(function(){
			$(this).parent().addClass('active')
		})
		$('.form-simple input').focusout(function(){
			 $(this).parent().removeClass('active')
		})
	}
}