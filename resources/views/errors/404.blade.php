@extends('layouts.app')

@section('content')

<section class="kopa-area kopa-area-05" style="padding-top: 300px;">
	<div class="container">
		<div class="widget kopa-widget-404">
			<div class="widget-content">
				<h2>WE ARE SORRY !</h2>
				<p>Can't find what you need? Take a moment and doa search below or start from our <a href="index-1.html">homepage.</a></p>
				<span>404</span>
				<form>
					<input type="text" placeholder="Search...">
					<button type="submit" class="button-01">send</button>
				</form>
			</div>
		</div>
	</div>
</section>

@endsection