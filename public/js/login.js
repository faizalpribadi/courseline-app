class Login extends App {

    index(){
        var data = {
            "username" : $('#identify').val(),
            "password" : $('#password').val(),
            "grant_type" : "password",
            "client_id" : 2,
            "client_secret" : "JmGXmahCSj9h4gU4l1rru4HKbhypIEJ9bb5jnwsL"
        }

        $.post("api/oauth/token", data, function(response){
            localStorage.setItem('access_token', response['access_token'])
            window.location.href = "{{ url('api/oauth/role') }}";
        }).fail(function(err){
            swal('Username / Password Salah', 'cek kembali username/password', 'error').then(function(){
                window.location.reload()
            })
        })
    }

}

$(document).ready(function(){
    
    var login = new Login;

    if(login.checkToken()){
        login.toDashboard()
    }

    login.inputInteractive()

    $('#formLogin').submit(function(e){
        e.preventDefault()
        login.index();
    })

})