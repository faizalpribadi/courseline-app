@extends('layouts.app')

@section('style')
@endsection

@section('content')
				<section class="kopa-area-nospace kopa-slider-desktop">
					<div class="widget kopa-widget-slider">
						<div class="widget-content module-slider-01 bg-image">
							<div class="container">
								<div class="content-box">

									<h1><b>Belajar di mana saja </b></h1>
									<h3>Di courseline kamu bisa belajar di rumah, sekolah, kampus, kantor, mall, kafe, dan di mana saja selama kamu punya akses internet.</h3>
									<div class="img-banner">
										<img src="img/text.png">
									</div>

									<div class="content-bot">
										<p>Education is learning what you didn't even know didn't know</p>
										<a href="#" class="button-01">Start Course</a>
										<a href="#" class="button-01">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="kopa-area-nospace">
					<div class="container">
						<div class="widget kopa-widget-advanced_search">
							<div class="widget-content module-advanced_search-01">
								<style type="text/css">
									.custom-bg::before{
										background-image:none !important;
									}

									.kopa-widget-advanced_search .module-advanced_search-01:before,
									.kopa-widget-advanced_search .module-advanced_search-01:after{
										border-top-color: #2c3e50 !important;
									}
								</style>
								<div class="custom-bg-0"></div>
								<div class="custom-bg">
									<form>
										<p class="arrow-select">
											<select name="course-length">
												<option selected> All Course</option>
												<option>Vocaline</option>
												<option>Businessline</option>
												<option>Schooline</option>
												<option>Languageline</option>
											</select>
										</p>
										<p class="arrow-select">
											<select name="study-level">
												<option>All Level</option>
												<option>Beginner</option>
												<option>Intermediate</option>
												<option>Advanced</option>
												<option>Expert</option>
											</select>
										</p>
										<p><input type="text" placeholder="Keywords..."></p>
										<p><button type="submit" class="button-01">Search Course</button></p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="kopa-area kopa-area-01">
					<div class="container">
						<div class="widget kopa-widget-features">
							<header class="widget-header style-01">
								<img src="img/icons/1.png" alt="">
								<h4>About Us</h4>
							</header>
							<div class="widget-content module-features-01-cs">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1 text-center">
										<div class="row">
											<p><b><i>COURSELINE</i></b> merupakan sebuah one-stop learning center yang memadukan program kursus dan teknologi, di mana pengajar dan murid dapat melakukan program kursus ini di mana saja dan kapan saja (on line course based).</p>

											<p>Keberadaan <i>COURSELINE</i> diharapkan dapat membantu setiap orang untuk mengembangkan bakat, potensi, intelegensi dan kemampuannya masing-masing tanpa ada kendala jarak dan waktu, sehingga COURSELINER (Peserta Didik Courseline) dapat bekerja sesuai dengan passion dan COURSELINER dapat berhasil dan dapat mencapai impiannya.</p>

											<p>Saksikan video berikut ini atau klik ALL ABOUT US untuk informasi selengkapnya mengenai <i>COURSELINE</i>.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="kopa-area kopa-ara-03" style="border:1px solid;background: url('img/bg1.jpg');background-size: 100%;background-attachment: fixed;">
					<div class="container">
						<div class="widget kopa-widget-services">
							<div class="widget-content module-services-01">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="kopa-item-services style-01">
													<div class="content">
														<img src="img/icons/schooline.png" alt="" width="70">
														<a href="#">Schooline</a>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="kopa-item-services style-02">
													<div class="content">
														<img src="img/icons/languageline.png" alt="" width="70">
														<a href="#">Languageline</a>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="kopa-item-services style-03">
													<div class="content">
														<img src="img/icons/vocaline.svg" alt="" width="70">
														<a href="#">Vocaline</a>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="kopa-item-services style-04">
													<div class="content">
														<img src="img/icons/businessline.png" alt="" width="70">
														<a href="#">Businessline</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="kopa-item-services style-05">
											<div class="content">
												<img src="img/1.jpg" alt="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area kopa-area-01">
					<div class="container">
						<div class="widget kopa-widget-features">
							<div class="widget-content module-features-01 module-features-02-cs">
								<div class="row">

									<div class="col-md-6 col-sm-12 col-xs-12 text-center">
										<img src="img/training.png" width="70%">
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h1>Corporate Course Service</h1>
											<p style="text-align: justify;margin-top: 20px;">Sadarkah kamu bahwa setiap orang memiliki potensi yang hebat dan dashyat dalam dirinya? Banyak orang dan mungkin termasuk kamu yang sudah menyadari hal ini. Namun untuk menjadi orang yang sukses dan hebat, menyadari saja tidak cukup, kamu perlu mengembangkan potensi kamu dengan belajar dan berlatih dengan tekun. Di COURSELINE, kamu dapat belajar dan berlatih dengan lebih flexible, lebih seru serta lebih kekinian. Ayo kembangkan potensi kamu bersama COURSELINE.</p>
										<a href="#" class="button-01" style="padding:10px 20px;margin-top: 30px;">view detail</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="kopa-area kopa-area-01">
					<div class="container">
						<div class="widget kopa-widget-features">
							<div class="widget-content module-features-01 module-features-02-cs">
								<div class="row">

									<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 text-center">
										<img src="img/teaching.png" width="80%">
									</div>

									<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12">
										<h1>Mengajar Di Courseline</h1>
											<p style="text-align: justify;margin-top: 20px;">Sadarkah kamu bahwa setiap orang memiliki potensi yang hebat dan dashyat dalam dirinya? Banyak orang dan mungkin termasuk kamu yang sudah menyadari hal ini. Namun untuk menjadi orang yang sukses dan hebat, menyadari saja tidak cukup, kamu perlu mengembangkan potensi kamu dengan belajar dan berlatih dengan tekun. Di COURSELINE, kamu dapat belajar dan berlatih dengan lebih flexible, lebih seru serta lebih kekinian. Ayo kembangkan potensi kamu bersama COURSELINE.</p>
										<a href="#" class="button-01" style="padding:10px 20px;margin-top: 30px;">Daftar</a>
										<a href="#" class="button-01" style="padding:10px 20px;margin-top: 30px;margin-left: 10px;">View More</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="kopa-area kopa-area-01">
					<div class="container">
						<div class="widget kopa-widget-features">
							<header class="widget-header style-01">
								<img src="img/icons/4.png" alt="">
								<h4>Our Services</h4>
							</header>
							<div class="widget-content module-features-01 module-features-02-cs">
								<div class="row">

									<div class="col-md-4 col-sm-12 col-xs-12">
										<article class="entry-item">
											<div class="entry-thumb">
												<img src="img/icons/2.png" alt="">
											</div>
											<div class="entry-content">
												<h4 class="entry-title">
													<a href="#">Meningkatkan Intelegensimu</a>
												</h4>
												<p style="text-align: justify;">Mahkota kamu sebagai orang yang bijak adalah intelegensi atau pengetahuanmu. Dengan banyak belajar, kamu akan memiliki banyak pengetahuan. Dengan banyaknya pengetahuan yang kamu miliki, kamu akan menjadi orang yang cerdas dan cerdik, karena orang yang cerdas dan cerdik bertindak dengan pengetahuan. Kamu pasti bisa berprestasi serta mencapai impian dan cita-cita kamu apabila kamu terus meningkatkan intelegensi kamu. Ayo tingkatkan intelegensi kamu di mana saja dan kapan saja bersama COURSELINE.</p>
												<a href="#" class="button-01">view detail</a>
											</div>
										</article>
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<article class="entry-item">
											<div class="entry-thumb">
												<img src="img/icons/3.png" alt="">
											</div>
											<div class="entry-content">
												<h4 class="entry-title">
													<a href="#">Mengembangkan Potensimu</a>
												</h4>
												<p style="text-align: justify;">Sadarkah kamu bahwa setiap orang memiliki potensi yang hebat dan dashyat dalam dirinya? Banyak orang dan mungkin termasuk kamu yang sudah menyadari hal ini. Namun untuk menjadi orang yang sukses dan hebat, menyadari saja tidak cukup, kamu perlu mengembangkan potensi kamu dengan belajar dan berlatih dengan tekun. Di COURSELINE, kamu dapat belajar dan berlatih dengan lebih flexible, lebih seru serta lebih kekinian. Ayo kembangkan potensi kamu bersama COURSELINE.</p>
												<a href="#" class="button-01">view detail</a>
											</div>
										</article>
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<article class="entry-item">
											<div class="entry-thumb">
												<img src="img/icons/5.png" alt="">
											</div>
											<div class="entry-content">
												<h4 class="entry-title">
													<a href="#">Mengasah Bakatmu</a>
												</h4>
												<p style="text-align: justify;">Saat kamu terlahir di dunia kamu pasti dianugerahi talent atau bakat oleh Sang Pencipta, Tuhan yang Maha Esa. Bakat tersebut akan berkembang dan menjadi maksimal apabila kamu mengasah bakat tersebut melalui pembelajaran dan pelatihan. Kalau dahulu jarak dan waktu jadi hambatan untuk kamu dalam mengembangkan dan mengasah bakat kamu, sekarang tidak lagi. Dengan COURSELINE, kamu bisa terus mengembangkan dan mengasah bakat kamu di mana saja dan kapan saja dengan guru atau tutor yang ahli dalam bidangnya, berpengalaman dan professional.</p>
												<a href="#" class="button-01">view detail</a>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area kopa-area-02 white-text-style" style="background: url('img/bg1.jpg');background-attachment: fixed;background-size: 100%;">
						<div class="container">
							<div class="widget kopa-widget-listcourse">
								<div class="widget-content module-listcourse-04">
									<div class="row">

										<div class="text-center">
											<h1 style="color: white;font-weight: bold;">Contact Us</h1>
											<hr style="width: 20%;">
										</div>
										<div class="col-sm-6 col-xs-12">

											<style type="text/css">
												.form-group{
													margin-bottom: 40px;
												}
												label{
													color: rgba(255,255,255,.8);
													margin-bottom: 10px;
												}

												label span{
													color: red !important;
												}
											</style>
											<form>
												<div class="form-group">
													<label>Nama Lengkap <span>*</span></label>
													<input type="text" name="" class="form-control" />
												</div>
												<div class="form-group">
													<label>Email <span>*</span></label>
													<input type="text" name="" class="form-control" />
												</div>
												<div class="form-group">
													<label>No. Whatsapp <span>*</span></label>
													<input type="text" name="" class="form-control" />
												</div>
												<div class="form-group">
													<label>Subjek <span>*</span></label>
													<input type="text" name="" class="form-control" />
												</div>
												<div class="form-group">
													<label>Pesan <span>*</span></label>
													<textarea class="form-control" rows="10">
													</textarea>
												</div>

												<div class="form-group">
													<button class="button-01"> Send </button>
												</div>
											</form>
										</div>

										<div class="col-sm-6 col-xs-12">
											<div style="margin-bottom: 40px;">
												<h3>Alamat</h3>
												<p>DBS Bank Tower, Ciputra World 1, 28th Floor, Jl. Prof. Dr. Satrio Kav 3-5 Jakarta, 12940 Indonesia, Jakarta Selatan, Jakarta, 12940, Indonesia</p>
											</div>

											<div style="margin-bottom: 40px;">
												<h3>Telepon</h3>
												<p style="font-size: 15px;"> <i class="fa fa-phone" style="margin-right: 10px;"></i> 021 29887948</p>
											</div>

											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.300357516523!2d106.82318234984822!3d-6.224070162667166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d16a0851224ea99!2sDBS+Bank+Tower!5e0!3m2!1sid!2sid!4v1526281571024" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
@endsection

@section('scripts')
@endsection