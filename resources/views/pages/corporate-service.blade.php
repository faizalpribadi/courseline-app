
@extends('layouts.app')
@section('content')

				<section class="kopa-area kopa-area-breadcrumb">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">Corporate Service</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span class="current-page">Corporate Service</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area">
					<div class="container">
						<div class="widget kopa-widget-course_info">
							<div class="widget-content module-course_info-02">
							<div class="row">
									<div class="col-sm-10 col-sm-offset-1 text-center">
										<img src="{{ asset('img/corporate-services.png') }}" style="margin-bottom:50px;">
										<p>Kualitas karyawan sangat mempengaruhi kualitas dan kesuksesan perusahaan. Semakin banyak karyawan yang berkualitas maka semakin baik dan semakin sukses perusahaan tersebut. Oleh karena itu, perusahaan yang besar dan sukses atau perusahaan yang ingin perusahaannya semakin besar dan semakin sukses, umumnya memiliki program khusus untuk pengembangan diri bagi karyawan-karyawannya. Ada perusahaan yang mengadakan in-house training, ada juga yang mengirimkan perwakilan karyawannya untuk mengikuti training di luar kantor, luar kota atau bahkan luar negeri.</p>
										<p>Kami menyadari akan betapa pentingnya pengembangan diri karyawan terhadap kesuksesan perusahaan. Kami jug sangat senang membuat karyawan-karyawan semakin berkualitas, sehingga perusahaan semakin baik dan semakin sukses. Oleh karena itu, kami menyediakan layanan virtual/online learning untuk corporate atau perusahaan.</p>
										<a href="{{ url('contact-us') }}" class="button-01" style="margin-top: 50px;">Contact Us For More Info</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

@endsection