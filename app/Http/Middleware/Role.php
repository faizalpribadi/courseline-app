<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles=[])
    {
        $role = Auth::user();
        $roles = [$roles];
        if(!in_array($role->roles_id, $roles)){
            return response()->json(['message'=>'Unauthorized'], 401);
        }

        return $next($request);
    }
}
