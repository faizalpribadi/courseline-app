<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseLevel;

class CourseLevelController extends Controller
{

	protected $table = 'course_level';

	public function __construct(){
	}

	public function store(Request $request){


		$data = new CourseLevel;

		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('insert', true, $data);
		}

		return $this->httpResponse('insert', false);
	}

	public function update(Request $request, $id){

		$data = CourseLevel::find($id);

		$data->name = $request->name;

		if($data->save()){
			return $this->httpResponse('update', true, $data);
		}

		return $this->httpResponse('update', false);
	}

}
