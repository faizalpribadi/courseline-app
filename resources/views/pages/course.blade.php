<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Courseline - Belajar menjadi lebih mudah</title>
		
		<!-- Font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700|Roboto+Slab:300,400,700" rel="stylesheet">

		<!-- Css Plugin -->
		<link rel="stylesheet" href="public/css/bootstrap.min.css">
		<link rel="stylesheet" href="public/css/font-awesome.min.css">
		<link rel="stylesheet" href="public/css/themify-icons.css">
		<link rel="stylesheet" href="public/css/owl.carousel.css">
		<link rel="stylesheet" href="public/css/owl.theme.css">
		<link rel="stylesheet" href="public/css/owl.transitions.css">
		<link rel="stylesheet" href="public/css/slick.css">
		<link rel="stylesheet" href="public/css/slider-pro.min.css">
		<link rel="stylesheet" href="public/css/jquery.mCustomScrollbar.css">
		<link rel="stylesheet" href="public/css/animate.css">
		<link rel="stylesheet" href="public/css/style.css">
		<link rel="stylesheet" type="text/css" href="public/css/extra-style.css">
		
		<!-- View Responsive -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

	</head>
	<body>
		<div id="main-container">
			<header class="kopa-header-01">
				<div class="container">
					<div class="top-header">
						<div class="kopa-search-form">
							<input type="text" name="search" placeholder="SEARCH" class="search-input">
						</div>
						<div class="kopa-login">
							<a href="login.html">
								<i class="fa fa-sign-in"></i>
								login
							</a>
						</div>
						<div class="kopa-register">
							<a href="register.html">
								<i class="fa fa-sign-in"></i>
								Register
							</a>
						</div>
					</div>
					<div class="bottom-header style-01">
						<div class="clearfix">
							<div class="pull-left">
								<h1 class="kopa-logo">
									<a href="#">
										<img src="public/assets/logo.png">
									</a>
								</h1>
							</div>
							<div class="pull-right">
								<nav class="kopa-main-menu style-01">
									<ul class="navbar-menu">
										<li class="menu-item-has-children current-menu-item">
											<a href="index.html">Home</a>
										</li>
										<li class="menu-item-has-children">
											<a href="about-us.html">About Us</a>
										</li>
										<li class="menu-item-has-children">
											<a href="#">Private Service</a>
											<ul style="margin-top: -10px;">
												<li><a href="vocaline.html">Vocaline</a></li>
												<li><a href="schooline.html">Schooline</a></li>
												<li><a href="languageline.html">Languageline</a></li>
												<li><a href="businessline.html">Businessline</a></li>
											</ul>
										</li>
										<li class="menu-item-has-children">
											<a href="corporate-service.html">Corporate Service</a>
										</li>
										<li class="border-bold">
											<a href="menjadi-pengajar.html">Menjadi Pengajar</a>
										</li>
										<li class="menu-item-has-children">
											<a href="contact-us.html">Contact Us</a>
										</li>
										<li class="menu-item-has-children">
											<a href="blog.html">Blog</a>
										</li>
									</ul>
								</nav>
								<div class="kopa-hamburger-menu">
									<i class="fa fa-bars"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div id="kopa-menu-scroll">
		        <div class="kopa-close-menu-scroll">
		            <span>close x</span>
		        </div>
		        <div class="top-menu-scroll">
		        	<div class="kopa-social-links style-03">
					    <ul class="clearfix">
					        <li><span>Social</span></li>
					        <li><a href="#" class="fa fa-twitter"></a></li>
					        <li><a href="#" class="fa fa-google-plus"></a></li>
					        <li><a href="#" class="fa fa-rss"></a></li>
					    </ul>
					</div>
			        <ul class="kopa-menu-click">
			            <li>
			            	<a href="index.html">Home</a>
			            </li>
			            <li>
			            	<a href="about-us.html">About us</a>
			            </li>
			            <li class="menu-item-has-children">
							<a href="#">Private Service</a>
							<ul>
								<li><a href="vocaline.html">Vocaline</a></li>
								<li><a href="businessline.html">Businessline</a></li>
								<li><a href="languageline.html">Languageline</a></li>
								<li><a href="schooline.html">Schooline</a></li>
							</ul>
						</li>
			            <li>
							<a href="corporate-service.html">Corporate Service</a>
						</li>
			            <li>
							<a href="menjadi-pengajar.html">Menjadi Pengajar</a>
						</li>
			            <li>
							<a href="contact-us.html">Contact Us</a>
						</li>
			            <li>
							<a href="blog.html">Blog</a>
						</li>
			        </ul>
		        </div>
		        <form>
		        	<button type="submit" class="btn"><i class="fa fa-search"></i></button>
		            <input type="text" placeholder="Search courses">
		        </form>
		        <div class="widget kopa-widget-banner">
		        	<div class="widget-content module-banner-01">
		        		<ul class="kopa-banner">
				        	<li>
				        		<span>100%</span>
				        		<div>
				        			<h4>MONEY BACK GUARANTEE</h4>
				        			<p>No hassles, no question asked!</p>
				        		</div>
				        	</li>
				        	<li>
				        		<h4>EARN UP TO <span>35%</span></h4>
				        		<p>Sign up our affiliate program</p>
				        	</li>
				        </ul>
		        	</div>
		        </div>
		    </div>
			<div id="main-content">

				<section class="kopa-area kopa-area-breadcrumb" style="padding-top: 300px;background:url('assets/2.jpg');background-attachment: fixed;background-size: 100%;">
					<div class="container">
						<div class="widget kopa-widget-breadcrumb">
							<div class="widget-content">
								<h2 style="color: white;font-weight: bold;">Course</h2>
								<div class="breadcrumb-nav">
									<span>
										<a href="#">
											<span>Home</span>
										</a>
									</span>
									<span class="current-page">Course</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="kopa-area kopa-area-17">
					<div class="container">
						<div class="widget kopa-widget-advanced_search">
							<div class="widget-content module-advanced_search-02">
								<form>
									<p class="arrow-select">
										<select name="course-length">
											<option value="*">Course Length</option>
											<option value="1">Course Length 1</option>
											<option value="2">Course Length 2</option>
											<option value="3">Course Length 3</option>
											<option value="4">Course Length 4</option>
											<option value="5">Course Length 5</option>
										</select>
									</p>
									<p class="arrow-select">
										<select name="study-level">
											<option value="*">Study Level</option>
											<option value="1">Study Level 1</option>
											<option value="2">Study Level 2</option>
											<option value="3">Study Level 3</option>
											<option value="4">Study Level 4</option>
											<option value="5">Study Level 5</option>
										</select>
									</p>
									<p><input type="text" placeholder="Keywords..."></p>
									<p><button type="submit" class="button-01">Search Course</button></p>
								</form>		
							</div>
						</div>
						<div class="widget kopa-widget-listcourse">
							<div class="widget-content">
								<div class="kopa-tabs style-02">
									<div class="row">
										<div class="col-md-4 col-sm-12 col-xs-12">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
											    <li class="active">
											    	<a href="#language" aria-controls="language" role="tab" data-toggle="tab">
											    		<i class="fa fa-microphone"></i>
											    		<div>
											    			<h4>VOCALINE</h4>
											    			<span>Education is learning what you </span>
											    		</div>
											    	</a>
											    </li>
											    <li>
											    	<a href="#crative" aria-controls="crative" role="tab" data-toggle="tab">
											    		<img src="assets/icons/businessline.png" width="35" style="float: left;margin-left: 5px;margin-right: 15px;">
											    		<div>
											    			<h4>BUSINESSLINE</h4>
											    			<span>Education is learning what you </span>
											    		</div>
											    	</a>
											    </li>
											    <li>
											    	<a href="#marketing" aria-controls="marketing" role="tab" data-toggle="tab">
											    		<i class="fa fa-language"></i>
											    		<div>
											    			<h4>LANGUAGELINE</h4>
											    			<span>Education is learning what you </span>
											    		</div>
											    	</a>
											    </li>
											    <li>
											    	<a href="#social" aria-controls="social" role="tab" data-toggle="tab">
											    		<i class="fa fa-users"></i>
											    		<div>
											    			<h4>SCHOOLINE</h4>
											    			<span>Education is learning what you </span>
											    		</div>
											    	</a>
											    </li>
											</ul>
										</div>
										<div class="col-md-8 col-sm-12 col-xs-12">
											<!-- Tab panes -->
											<div class="tab-content">
											    <div class="tab-pane fade in active js-slick-2" id="language">
											    	<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 02</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 03</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 04</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 02</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 03</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 04</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
											    </div>
											    <div class="tab-pane fade js-slick-2" id="crative">
											    	<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 05</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 06</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 07</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 08</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 05</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 06</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 07</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 08</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
											    </div>
											    <div class="tab-pane fade js-slick-2" id="marketing">
											    	<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 01</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 05</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 06</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 07</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 08</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
											    </div>
											    <div class="tab-pane fade js-slick-2" id="social">
											    	<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 09</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 10</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 11</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 12</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 05</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 06</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 07</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 08</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
											    </div>
											    <div class="tab-pane fade js-slick-2" id="computer">
											    	<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 13</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 14</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 15</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 16</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 05</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 06</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 07</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
													<article class="entry-item kopa-item-course-02">
														<div class="entry-thumb">
															<a href="#">
																<img src="http://placehold.it/300x231" alt="">
																<i class="fa fa-link"></i>
															</a>
															<span class="course-category">Language</span>
														</div>
														<div class="entry-content">
															
															<h4 class="entry-title">
																<a href="#">Histroy of  English LITERATURe 08</a>
															</h4>
															<a href="#" class="course-author">by Touhida Moni, Ast. Senior Trainer</a>
															<div class="course-price">
																<span class="price">$ 229</span>
																<div class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																</div>
															</div>
															<ul class="course-detail">
																<li>
																	<i class="fa fa-clock-o"></i>
																	2 Years
																</li>
																<li>
																	<i class="fa fa-user"></i>
																	154
																</li>
																<li>
																	<i class="fa fa-comments-o"></i>
																	23
																</li>
															</ul>
														</div>
													</article>
											    </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="bottom-sidebar style-01 white-text-style">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12 col-md-push-4">
							<div class="widget kopa-widget-logof">
								<div class="widget-content">
									<a href="#"><img class="footer-img" src="img/extra/logo-white.png" alt=""></a>
									<div class="kopa-social-links style-02">
									    <ul class="clearfix">
									        <li><a href="#" class="fa fa-facebook"></a></li>
									        <li><a href="#" class="fa fa-twitter"></a></li>
									        <li><a href="#" class="fa fa-google-plus"></a></li>
									        <li><a href="#" class="fa fa-rss"></a></li>
									    </ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 col-md-pull-4">
							<div class="widget kopa-widget-tweets">
								<h3 class="widget-title style-02">RECENT TWEETS</h3>
								<div class="widget-content module-tweets-01">
									<ul>
										<li>
											<i class="fa fa-twitter"></i>
											<div class="tweets-detail">
												<a href="#">
													<span>@ASCETICDESIGNERS,</span>
													Next Jobs Fair Will Be Held in Our Campus.
												</a>
												<span>5 Mins Ago</span>
											</div>
										</li>
										<li>
											<i class="fa fa-twitter"></i>
											<div class="tweets-detail">
												<a href="#">
													<span>@ASHIFULPAPPU,</span>
													Build Your Career at Our Paathshaala Faculties.
												</a>
												<span>25 Mins Ago</span>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="widget kopa-widget-useful">
								<h3 class="widget-title style-03">useful links</h3>
								<div class="widget-content">
									<ul>
										<li><a href="#">All Our Course</a></li>
										<li><a href="#"> Private Service </a></li>
										<li><a href="#">Corporate Service</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Terms &amp; Conditions</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="kopa-footer style-01">
				<div class="container">
					<p>© 2018 <a href="#">Digikom</a>  |  All Rights Reserved</p>
				</div>
			</footer>

		</div>

		<script src="public/js/jquery-2.2.4.min.js"></script>
		<script src="public/js/bootstrap.min.js"></script>
		<script src="public/js/slick.min.js"></script>
		<script src="public/js/jquery.sliderPro.min.js"></script>
		<script src="public/js/imagesloaded.pkgd.min.js"></script>
		<script src="public/js/jquery.validate.min.js"></script>
		<script src="public/js/owl.carousel.min.js"></script>
		<script src="public/js/jquery.waypoints.min.js"></script>
		<script src="public/js/jquery.counterup.min.js"></script>
		<script src="public/js/jquery.mCustomScrollbar.min.js"></script>
		<script src="public/js/viewportchecker.min.js"></script>
		<script src="public/js/custom.js"></script>
	
</body>
</html>